const merge = require('webpack-merge')
const baseWebpackConfig = require('./build/webpack.base.conf')

module.exports = {
    webpackConfig: merge(
        baseWebpackConfig,
      {}
    )
  };