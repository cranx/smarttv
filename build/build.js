'use strict'
require('./check-versions')()

process.env.NODE_ENV = 'production'

const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')

const args = process.argv.slice(2)

process.env.WATCH = args.some(v => v === 'watch')

const platforms = [
  'tizen',
  'webos'
]

function clearRequireCache() {
  Object.keys(require.cache).forEach(function(key) {
    delete require.cache[key];
  });  
}

function selectPlatform(platform) {
    process.env.PLATFORM = platform
    var config = require('../config')
    var webpackConfig = require('./webpack.prod.conf')
    clearRequireCache()
    console.log(chalk.cyan('  Build platform:', platform, '\n'))
    return { config: config, webpackConfig: webpackConfig }
} 

function clearDir(assetsRoot, assetsSubDirectory){
  return new Promise((res, rej)=>{
    rm(path.join(assetsRoot, assetsSubDirectory), err => {
      if (err) rej(err)
      res()
    })
  })
}

function generate(cfg){
    webpack(cfg,(err, stats) => {
      if (err) throw err
      process.stdout.write(stats.toString({
        colors: true,
        modules: false,
        children: false, // If you are using ts-loader, setting this to true will make TypeScript errors show up during build.
        chunks: false,
        chunkModules: false
      }) + '\n\n')

      if (stats.hasErrors()) {
        console.log(chalk.red('  Build failed with errors.\n'))
        process.exit(1)
      }

      console.log(chalk.cyan('  Build complete.\n'))
      console.log(chalk.yellow(
        '  Tip: built files are meant to be served over an HTTP server.\n' +
        '  Opening index.html over file:// won\'t work.\n'
      ))
      if (process.env.WATCH === "true") 
        console.log(chalk.greenBright('  With watch.\n'))
    })
}

var builder = (platforms, args, selectPlatform, clearDir, generate) => {
  var wpcfg;
  if (args.indexOf('all') >= 0) {
    wpcfg = platforms.map((platform)=>{
      var {config, webpackConfig} = selectPlatform(platform)
      clearDir(config.build.assetsRoot, config.build.assetsSubDirectory).catch((e)=>console.log(e))
      return webpackConfig
    })
  } else {
    let platform = platforms.find(v => args.indexOf(v) >= 0) || platforms[0]
    var {config, webpackConfig} = selectPlatform(platform)
    clearDir(config.build.assetsRoot, config.build.assetsSubDirectory).catch((e)=>console.log(e))
    wpcfg = webpackConfig
  }
  return generate(wpcfg)
}

builder(platforms, args, selectPlatform, clearDir, generate)



