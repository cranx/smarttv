# app-vue

[![pipeline status](https://gitlab.rambler.ru/oksat/tv/app-vue/badges/master/pipeline.svg)](https://gitlab.rambler.ru/oksat/tv/app-vue/commits/master)
[![coverage report](https://gitlab.rambler.ru/oksat/tv/app-vue/badges/master/coverage.svg)](https://gitlab.rambler.ru/oksat/tv/app-vue/commits/master)

> A Vue.js project

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# build for production with minification for select platform
npm run build tizen (tizen|webos)

# build for production with minification for all platform
npm run build all

# build for production with minification and watch changes for tizen platform 
npm run build watch

# build for production with minification and watch changes for webos platform 
npm run build webos watch
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
