const ua = navigator.userAgent.toLowerCase()
let platform = {
  name: 'browser'
}

export default {
  install: (Vue, options) => {
    if (ua.includes('tizen')) {
      const tizen = window.tizen

      platform = {
        name: 'tizen',
        api: tizen
      }

      tizen.tvinputdevice.registerKey('MediaPlayPause')
      tizen.tvinputdevice.registerKey('MediaPlay')
      tizen.tvinputdevice.registerKey('MediaStop')
      tizen.tvinputdevice.registerKey('MediaPause')
      tizen.tvinputdevice.registerKey('MediaRewind')
      tizen.tvinputdevice.registerKey('MediaFastForward')

      tizen.tvinputdevice.registerKey('ColorF0Red')
      tizen.tvinputdevice.registerKey('ColorF1Green')
      tizen.tvinputdevice.registerKey('ColorF2Yellow')
      tizen.tvinputdevice.registerKey('ColorF3Blue')

      let value = tizen.tvinputdevice.getSupportedKeys()
      console.log(value)
    }

    Object.defineProperty(Vue.prototype, '$platform', {
      get() {
        return platform
      }
    })
  }
}
