export default {
  namespaced: true,
  state: {
    locale: null,
    translations: {}
  },

  getters: {
    // progress(state) {
    //   return state.currentTime / state.duration * 100
    // }
  },

  mutations: {
    SET_LOCALE(state, payload) {
      state.translations = payload.translations
    }
  },

  actions: {
    setLocale({ commit }, payload) {
      commit({
        type: 'SET_LOCALE',
        translations: payload.translations
      })
    }
  }
}
