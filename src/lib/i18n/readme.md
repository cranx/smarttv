## Подключение

```js
import Vue from 'vue'
import store from './store/index'
import I18nPlugin from './lib/I18nPlugin'

Vue.use(I18nPlugin, store)
```

## Инициализация

```js
this.$i18n.set({
  language: 'язык',
  filmsPluralize: 'фильм|фильма|фильмов'
})
```

## Использование

### Замена теста

```js
<div v-i18n:language /> // язык
```

### Плюрализация теста

```js
<div v-i18n:filmsPluralize.plural data-number="26" /> // фильмов
```
