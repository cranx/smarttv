import storeModule from './i18nStore'

export default {
  install: (Vue, store, options) => {
    // Подключение vuex store
    store.registerModule('i18n', storeModule)

    /*
    * Подключение глобальноой директивы $i18n
    */

    /**
     * Изменение текста внутри элемента текстом сохраненным в vuex store
     * @param {Object} el
     * @param {Object} binding
     * @param {string} binding.arg
     */
    let translateIn = (el, binding) => {
      let text = store.state['i18n'].translations[binding.arg]
      if (!text || el.textContent === text) return

      if (binding.modifiers.plural)
        text = el.dataset.number
          ? pluralize(el.dataset.number, text.split('|'))
          : text

      el.textContent = text
    }

    /**
     * Pluralize
     * @param {Number} number
     * @param {Array} titles
     */
    let pluralize = (number, titles) => {
      let cases = [2, 0, 1, 1, 1, 2]
      let elem =
        number % 100 > 4 && number % 100 < 20
          ? 2
          : cases[number % 10 < 5 ? number % 10 : 5]
      return titles[elem]
    }

    Vue.directive('i18n', {
      bind: translateIn,
      // inserted: (el, binding, vnode, oldVnode) => {
      //   console.log('inserted')
      // },
      update: translateIn
      // componentUpdated: (el, binding, vnode, oldVnode) => {
      //   console.log('componentUpdated', el)
      // },
      // unbind: (el, binding, vnode, oldVnode) => {
      //   console.log('unbind')
      // }
    })

    /*
    * Подключение глобального метода $i18n
    */

    // Обработчики взаимодействия с локальным хранилищем
    let setLocale = translations =>
      store.dispatch({
        type: 'i18n/setLocale',
        translations: translations
      })

    Object.defineProperty(Vue.prototype, '$i18n', {
      get() {
        return {
          set: setLocale
        }
      }
    })
  }
}
