export default class FocusManager {
  current = null
  keys = null
  screenHistory = []
  longPress = {}

  init(app, keys) {
    this.keys = keys
    this.app = app

    window.addEventListener('keyup', e => {
      if (this.longPress[e.which] === 'caught') {
        this.longPress[e.which] = null
        return
      } else if (this.longPress[e.which]) {
        clearTimeout(this.longPress[e.which])
        this.longPress[e.which] = null
      }

      // Позволяет установить фокус на элементе
      if (!this.current) {
        this.getCurrentScreen(app).onFocus()
        this.getCurrentScreen(app).$emit('keypress', e)
      }

      if (e.which === keys.LEFT) {
        this.navigate('left')
      } else if (e.which === keys.RIGHT) {
        this.navigate('right')
      } else if (e.which === keys.UP) {
        this.navigate('up')
      } else if (e.which === keys.DOWN) {
        this.navigate('down')
      } else if (e.which === keys.ENTER) {
        this.current.$emit('action', {})
      } else if (e.which === keys.BACK) {
        this.getCurrentScreen(app).handleBack()
      }

      this.getCurrentScreen(app).$emit('keypress', e)
    })

    window.addEventListener('keydown', e => {
      if (this.longPress[e.which]) return

      this.longPress[e.which] = setTimeout(() => {
        this.getCurrentScreen(app).$emit('longpress', e)
        this.longPress[e.which] = 'caught'
      }, 500)
    })
  }

  getCurrentScreen(app) {
    // Фокус на элементе вне сцены (Popup, ...)
    if (app.currentFocusArea) {
      return app.currentFocusArea
    }
    // Фокус на элементе активной сцены
    return app.currentScreen
  }

  navigate(dir) {
    const current = this.current

    if (!current) return

    if (current[dir] && typeof current[dir].focus === 'function') {
      // direct component ref
      current[dir].focus()
    } else if (current[dir] === null) {
      // if NULL is set then just emit event
      current.$emit('nav_' + dir)
    } else {
      // spatial navigation
      let children = current.$parent.$children.filter(item => !!item.focus)
      let navTo = this.spatial(current, dir, children)

      if (navTo) {
        navTo.focus()
      }
    }
  }

  showScreen(routeOptions) {
    let route = this.app.$route

    // save prev screen
    this.screenHistory.push(route)

    // route to next
    this.app.$router.push(routeOptions)
  }

  showPrevScreen() {
    if (this.screenHistory.length) {
      let prev = this.screenHistory.pop()

      this.app.$router.replace({
        name: prev.name,
        params: prev.params
      })
    }
  }

  setFocus(newItem) {
    if (this.current) {
      this.current.focused = false
    }

    this.current = newItem

    if (newItem) {
      this.current.focused = true
    }
  }

  spatial(item, dir, navs) {
    let element = item.$el
    let objBounds = element.getBoundingClientRect()
    let arr = []
    let curBounds = null
    let cond1
    let cond2

    for (let i = 0, l = navs.length; i < l; i++) {
      curBounds = navs[i].$el.getBoundingClientRect()

      if (
        curBounds.left === objBounds.left &&
        curBounds.top === objBounds.top
      ) {
        continue
      }

      switch (dir) {
        case 'left':
          cond1 = objBounds.left > curBounds.left
          break
        case 'right':
          cond1 = objBounds.right < curBounds.right
          break
        case 'up':
          cond1 = objBounds.top > curBounds.top
          break
        case 'down':
          cond1 = objBounds.bottom < curBounds.bottom
          break
        default:
          break
      }

      if (cond1) {
        arr.push({
          obj: navs[i],
          bounds: curBounds
        })
      }
    }

    /* eslint-disable no-unused-vars */
    let minDy = 9999999
    let minDx = 9999999
    let minD = 9999999
    let maxIntersection = 0
    let dy = 0
    let dx = 0
    let d = 0
    let isIntersects = (b1, b2, dir) => {
      let temp = null
      switch (dir) {
        case 'left':
        case 'right':
          if (b1.top > b2.top) {
            temp = b2
            b2 = b1
            b1 = temp
          }
          if (b1.bottom > b2.top) {
            if (b1.top > b2.right) {
              return b2.top - b1.right
            } else {
              return b2.height
            }
          }
          break
        case 'up':
        case 'down':
          if (b1.left > b2.left) {
            temp = b2
            b2 = b1
            b1 = temp
          }
          if (b1.right > b2.left) {
            if (b1.left > b2.right) {
              return b2.left - b1.right
            } else {
              return b2.width
            }
          }
          break
        default:
          break
      }
      return false
    }

    let intersectsAny = false
    let found = false

    for (let i = 0, l = arr.length; i < l; i++) {
      let b = arr[i].bounds
      let intersects = isIntersects(objBounds, b, dir)
      dy = Math.abs(b.top - objBounds.top)
      dx = Math.abs(b.left - objBounds.left)
      d = Math.sqrt(dy * dy + dx * dx)
      if (intersectsAny && !intersects) {
        continue
      }
      if (intersects && !intersectsAny) {
        minDy = dy
        minDx = dx
        maxIntersection = intersects
        found = arr[i].obj
        intersectsAny = true
        continue
      }

      switch (dir) {
        case 'left':
        case 'right':
          if (intersectsAny) {
            cond2 = dx < minDx || (dx === minDx && dy < minDy)
          } else {
            cond2 = dy < minDy || (dy === minDy && dx < minDx)
          }
          break
        case 'up':
        case 'down':
          if (intersectsAny) {
            cond2 = dy < minDy || (dy === minDy && dx < minDx)
          } else {
            cond2 = dx < minDx || (dx === minDx && dy < minDy)
          }
          break
        default:
          break
      }
      if (cond2) {
        minDy = dy
        minDx = dx
        minD = d
        found = arr[i].obj
      }
    }

    return found
  }
}
