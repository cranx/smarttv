import FocusManager from './FocusManager'

const focus = new FocusManager()

export default {
  install: (Vue, options) => {
    Object.defineProperty(Vue.prototype, '$focus', {
      get() {
        return focus
      }
    })
  }
}
