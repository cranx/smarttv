export default class TVKeys {
  constructor(platform) {
    this.LEFT = 37
    this.UP = 38
    this.RIGHT = 39
    this.DOWN = 40
    this.ENTER = 13
    this.BACK = 8

    this.RED = 112
    this.GREEN = 113
    this.YELLOW = 114
    this.BLUE = 115

    this.RW = 188
    this.PAUSE = 187
    this.FF = 190
    this.PLAY = 189
    this.STOP = 48
    this.PLAYPAUSE = 191

    if (platform.name === 'tizen') {
      this.BACK = 10009
      this.RED = 403
      this.GREEN = 404
      this.YELLOW = 405
      this.BLUE = 406

      this.RW = 412
      this.PAUSE = 19
      this.FF = 417
      this.PLAY = 415
      this.STOP = 413
      this.PLAYPAUSE = 10252
    }
  }
}
