import { ApolloClient } from 'apollo-client'
import { onError } from 'apollo-link-error'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { ApolloLink } from 'apollo-link'
import { RetryLink } from 'apollo-link-retry'
import { getToken, setToken, removeToken, fetchToken } from '../auth/auth'

// https://www.apollographql.com/docs/link/links/http.html
/* global APOLLO_CLIENT_URL */
const httpLink = createHttpLink({
  uri: APOLLO_CLIENT_URL + '/graphql'
})

// Create the subscription websocket link
// const wsLink = new WebSocketLink({
//   uri: 'ws://localhost:3000/subscriptions',
//   options: {
//     reconnect: true,
//   },
// })

// https://www.apollographql.com/docs/link/links/context.html
const authLink = setContext(({ operationName }, context) =>
  Promise.resolve(getToken())
    .then(token => {
      if (!token && !context.skipToken) {
        return fetchToken(apolloClient, operationName).then(token => {
          setToken(token)
          return token
        })
      }
      return token
    })
    .then(token => {
      if (token) {
        context.headers = {
          authorization: `Bearer ${token}`
        }
      }
      return context
    })
)

// https://www.apollographql.com/docs/link/links/error.html
const errorLink = onError(({ networkError }) => {
  if (networkError && networkError.statusCode === 401) {
    removeToken()
  }
})

// https://www.apollographql.com/docs/link/links/retry.html
const retryLink = new RetryLink()

// https://www.apollographql.com/docs/react/basics/setup.html#ApolloClient
const apolloClient = new ApolloClient({
  connectToDevTools: true,
  // https://www.apollographql.com/docs/link/overview.html
  link: ApolloLink.from([errorLink, authLink, retryLink, httpLink]),
  cache: new InMemoryCache()
})

// Для бытрой проверки подключения
// apolloClient
//   .query({
//     query: gql`
//       query records {
//         records {
//           id
//           name
//         }
//       }
//     `
//   })
//   .then(data => console.log(data))
//   .catch(error => console.error(error))

export { apolloClient }
