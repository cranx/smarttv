import CREATE_SCREEN_MUTATION from '@/graphql/CreateScreen.graphql'

export const getToken = () => window.localStorage.getItem('token')

export const setToken = token => window.localStorage.setItem('token', token)

export const removeToken = () => window.localStorage.removeItem('token')

export const fetchToken = client =>
  client
    .mutate({
      mutation: CREATE_SCREEN_MUTATION,
      fetchPolicy: 'no-cache',
      variables: {
        useragent: navigator.userAgent,
        platform: 'app-vue'
      },
      context: {
        skipToken: true
      }
    })
    .then(res => res.data.create_screen.token)
