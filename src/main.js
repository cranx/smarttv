// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill'
import 'unfetch/polyfill'
import Vue from 'vue'
import apolloProvider from './service/apollo'
import App from './App'
import VueRouter from 'vue-router'
import router from './router'
import NavItem from './components/ui/NavItem'
import platformPlugin from './lib/platformPlugin'
import focusManagerPlugin from './lib/focusManager'
import TVKeys from './lib/tvKeys'
import store from './store/index'
import i18nPlugin from './lib/i18n'

Vue.config.productionTip = false

// global components
Vue.component('nav-item', NavItem)

Vue.use(VueRouter)

Vue.use(platformPlugin)
Vue.use(focusManagerPlugin)
Vue.use(i18nPlugin, store)

let app = new Vue({
  el: '#app',
  render: h => h(App),
  store,
  router,
  provide: apolloProvider.provide()
})

app.$root.$focus.init(app, new TVKeys(app.$root.$platform))
