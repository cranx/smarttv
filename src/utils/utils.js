export default {
  formatMMSS(value) {
    let minutes = Math.floor(value / 60) || 0
    let seconds = Math.floor(value % 60) || 0

    if (minutes < 10) {
      minutes = '0' + minutes
    }
    if (seconds < 10) {
      seconds = '0' + seconds
    }

    return minutes + ':' + seconds
  }
}
