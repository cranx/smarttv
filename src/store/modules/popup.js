export default {
  namespaced: true,
  state: {
    theme: {
      alert: false
    }
  },

  mutations: {
    SHOW: (state, payload) => (state.theme[payload.name] = true),
    HIDE: (state, payload) => (state.theme[payload.name] = false)
  },

  actions: {
    show: ({ commit }, value) => commit({ type: 'SHOW', name: value }),
    hide: ({ commit }, value) => commit({ type: 'HIDE', name: value })
  }
}
