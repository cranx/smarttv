export default {
  namespaced: true,
  state: {
    currentTime: 0,
    duration: 0,
    buffered: 0
  },

  getters: {
    progress(state) {
      return state.currentTime / state.duration * 100
    }
  },

  mutations: {
    DURATION(store, value) {
      store.duration = value
    },
    CURRENT_TIME(store, value) {
      store.currentTime = value
    },
    BUFFERED(store, value) {
      store.buffered = value
    }
  },

  actions: {
    currentTime({ commit }, value) {
      commit('CURRENT_TIME', value)
    },
    duration({ commit }, value) {
      commit('DURATION', value)
    },
    buffered({ commit }, value) {
      commit('BUFFERED', value)
    }
  }
}
