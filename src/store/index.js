import Vuex from 'vuex'
import player from './modules/player'
import popup from './modules/popup'
import Vue from 'vue'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    player: player,
    popup: popup
  },
  getters: {},
  actions: {},
  mutations: {}
})

export default store
