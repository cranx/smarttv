export default {
  data() {
    return {
      defaultFocus: ''
    }
  },
  methods: {
    // Обязательный обработчик возврата к предыдущему экрану
    handleBack() {
      this.historyBack()
    },

    historyBack() {
      this.$focus.showPrevScreen()
    },
    onFocus() {
      if (this.defaultFocus) {
        this.$refs[this.defaultFocus].focus()
      } else {
        this.$children[0].focus()
      }
    },
    initScreen() {
      this.$root.currentScreen = this
      this.onFocus()

      this.$on('keypress', e => {
        if (e.which === this.$focus.keys.RED) {
          location.reload()
        }
      })
    },
    showPopup(name) {
      this.$store.dispatch('popup/show', name)
    },
    hidePopup(name) {
      this.$store.dispatch('popup/hide', name)
    }
  },
  mounted() {
    this.initScreen()
  }
}
