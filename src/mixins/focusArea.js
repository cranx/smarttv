export default {
  data: () => ({
    defaultFocus: ''
  }),
  methods: {
    // Обязательный обработчик возврата к предыдущему экрану
    handleBack() {
      // вызывается только при существующем currentFocusArea
      this.$root.currentFocusArea.close()
    },

    onFocus() {
      if (!this.defaultFocus)
        console.warn('defaultFocus is required', this.name)
      this.$refs[this.defaultFocus].focus()
    },
    initArea() {
      this.$root.currentFocusArea = this
      this.onFocus()
    },
    showPopup(name) {
      this.$store.dispatch('popup/show', name)
    },
    hidePopup(name) {
      this.$root.currentScreen.onFocus()
      this.$root.currentFocusArea = null
      this.$store.dispatch('popup/hide', name)
    }
  }
}
