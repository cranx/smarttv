import Router from 'vue-router'
import Trailers from '@/components/Trailers'

export default new Router({
  routes: [
    {
      path: '/',
      name: 'trailers',
      component: Trailers
    }
  ]
})
