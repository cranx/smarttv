const { promisify } = require('util')
const jwt = require('express-jwt')
const jsonwebtoken = require('jsonwebtoken')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
const express = require('express')
const bodyParser = require('body-parser')
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express')
const { makeExecutableSchema } = require('graphql-tools')
const HOST = process.argv.slice(2)[0] || 'localhost'
var cors = require('cors')

const data = require('./data.json')
const {
  GraphQLEmail,
  GraphQLURL,
  GraphQLDateTime,
  GraphQLLimitedString,
  GraphQLPassword,
  GraphQLUUID
} = require('graphql-custom-types')
const GraphQLJSON = require('graphql-type-json')
const EagleAPI = require('./eagleplatform')
const path = require('path')

const JWT_SECRET = 'asdsa'

// The GraphQL schema in string form
const typeDefs = fs.readFileSync(
  path.resolve(__dirname, 'types.graphql'),
  'utf8'
)

// The resolvers
const resolvers = {
  Email: GraphQLEmail,
  URL: GraphQLURL,
  DateTime: GraphQLDateTime,
  Username: new GraphQLLimitedString(
    3,
    50,
    'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890_-.'
  ),
  Password: new GraphQLPassword(3, 50),
  UUID: GraphQLUUID,
  JSON: GraphQLJSON,
  Query: {
    viewer: (root, args, context) => {
      if (context.viewer) {
        // return ApiConnector.getResource('users', context.viewer.id, context.requestId),
        return {
          id: context.viewer.userId,
          name: 'Fake user'
        }
      }

      return null
    },
    records: (root, args, context) => {
      return EagleAPI.getRecords()
    },
    record: (root, args, context) => {
      return EagleAPI.getRecord(args.id)
    },

    translations: (root, args, context) => {
      return context.api.call('json_store.document', {
        code: 'web-react',
        language: 'ru',
        published: true
      })
    },

    document: async (obj, args, context, info) => {
      return context.api.call('json_store.document', args)
    }
  },
  Mutation: {
    signIn: (obj, args, context, info) => {
      const sign = promisify(jsonwebtoken.sign)
      const userData = { userId: '11' }
      const token = sign(userData, JWT_SECRET)
      return { token }
    }
  },
  Viewer: {
    features: (root, args, context) => data.features
  },
  Collection: {
    movies: (root, args, context) =>
      data.movies.filter(movie => root.movies.includes(movie.id))
  }
}

const documents = require('./json-store-documents')
const api = {
  async call(action, payload) {
    switch (action) {
      case 'json_store.document':
        return documents[payload.code][payload.language]
      default:
        return null
    }
  }
}

// Put together a schema
const schema = makeExecutableSchema({
  typeDefs,
  resolvers
})

// Initialize the app
const app = express()

app.use(cors())
// The GraphQL endpoint
app.use(
  '/graphql',
  bodyParser.json(),
  jwt({
    secret: JWT_SECRET,
    credentialsRequired: false
  }),
  graphqlExpress(async req => {
    return {
      schema,
      context: {
        api,
        requestId: uuidv4(),
        userId: req.user ? req.user.id : 'guest',
        viewer: req.user
      }
    }
  })
)

// GraphiQL, a visual editor for queries
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }))

// Start the server
app.listen(3001, HOST, () => {
  console.log('Server start on http://' + HOST + ':3001')
  console.log('Go to http://localhost:3001/graphiql to run queries!')
})
