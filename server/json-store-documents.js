module.exports = {
  'web-react': {
    ru: {
      header: {
        menu: 'Меню',
        close: 'Скрыть',
        account: 'Аккаунт',
        search: 'Поиск',
        tv: 'Что посмотреть',
        playlist: 'Моя коллекция'
      },
      player: {
        prev: 'Назад',
        next: 'Следующее',
        play: 'Плей',
        pause: 'Пауза',
        watchFilm: 'Смотреть фильм',
        to_favorite: 'В коллекцию',
        in_favorites: 'В коллекции',
        filter: 'Фильтры',
        to_fullscreen: 'Развернуть экран',
        from_fullscreen: 'Свернуть экран',
        volume_on: 'Включить звук',
        volume_off: 'Выключить звук'
      }
    },
    en: {
      header: {
        menu: 'Menu',
        close: 'Hide',
        account: 'Account',
        search: 'Search',
        tv: 'What to see',
        playlist: 'My collection'
      },
      player: {
        prev: 'Back',
        next: 'Next',
        play: 'Play',
        pause: 'Pause',
        watchFilm: 'Watch movie',
        to_favorite: 'Add to collection',
        in_favorites: 'In the collection',
        filter: 'Filters',
        to_fullscreen: 'To fullscreen',
        from_fullscreen: 'From fullscreen',
        volume_on: 'Volume on',
        volume_off: 'Volume off'
      }
    }
  },
  'app-vue': {
    ru: {
      playerPrev: 'Назад',
      playerNext: 'Следующее',
      playerPlay: 'Плей',
      playerPause: 'Пауза',
      playerWatchFilm: 'Смотреть фильм',
      playerToFavorite: 'В коллекцию',
      playerInFavorites: 'В коллекции',
      playerFilter: 'Фильтры',
      playerVolumeOn: 'Включить звук',
      playerVolumeOff: 'Выключить звук',
      pluralizeTest: 'фильм|фильма|фильмов'
    },
    en: {
      playerPrev: 'Back',
      playerNext: 'Next',
      playerPlay: 'Play',
      playerPause: 'Pause',
      playerWatchFilm: 'Watch movie',
      playerToFavorite: 'Add to collection',
      playerInFavorites: 'In the collection',
      playerFilter: 'Filters',
      playerVolumeOn: 'Volume on',
      playerVolumeOff: 'Volume off',
      pluralizeTest: 'film|films'
    }
  }
}
