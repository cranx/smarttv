const rp = require('request-promise')
const LRU = require('lru-cache')
const cache = new LRU({ maxAge: 1000 * 60 })

const account = 'afisha'
const auth_token = 'b_fXFF5pWkz2XtfXb8zG' // eslint-disable-line camelcase

module.exports = {
  async getRecords() {
    if (!cache.has('records')) {
      const response = await rp({
        uri: `http://api.eagleplatform.com/media/filters/20784.json`,
        qs: {
          account,
          auth_token,
          pagelimit: 100,
          per_page: 100,
          sort: 'created_at',
          direction: 'minus'
        },
        json: true
      })

      console.log('fetch records')

      const records = response.data.records
        .filter(({ is_processed }) => Boolean(is_processed)) // eslint-disable-line camelcase
        .map(({ id, name }) => ({
          id,
          name
        }))

      cache.set('records', records)
    }

    return cache.get('records')
  },

  async getRecord(id) {
    if (!cache.has(id)) {
      const response = await rp({
        uri: `http://api.eagleplatform.com/media/records/${id}.json`,
        qs: {
          account,
          auth_token
        },
        json: true
      })

      console.log('fetch record ', id)

      const { name, record_files = [] } = response.data.record // eslint-disable-line camelcase

      const record = {
        id,
        name,
        record_files: record_files.map(({ width, height, size, url }) => ({
          width,
          height,
          size,
          url: url + '&account=afisha'
        }))
      }

      cache.set(id, record)
    }

    return cache.get(id)
  }
}
