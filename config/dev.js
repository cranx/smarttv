'use strict'
const merge = require('webpack-merge')
const prod = require('./prod')

module.exports = merge(prod, {
  env: {
    NODE_ENV: '"development"'
  },
  APOLLO_CLIENT_URL: 'http://0publicapi-back01.stage.oksat.rambler.tech:8080'
})
